package smartdoor.common;

import smartdoor.common.event.Event;

public interface Observer {

    
    boolean notifyEvent(final Event<?> ev);
}
