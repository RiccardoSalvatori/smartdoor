package smartdoor.common.event;

import java.util.Arrays;

/**
 * Codici per i messaggi tra arduino e raspberry
 *
 */
public enum MsgType {
    
    /**
     * Da rasperry ad arduino.
     */
    OK("1"), FAIL("0"),

    /**
     * Da arduino a raspberry.
     */
    LOG("[L]"), CHECK("CHECK"), NO_PRESENCE("NOPRES"), USER_INSIDE("INSIDE"), OFF("6"), UPDATE("5|");

    private final String code;

    /**
     * 
     * @param c
     *            stringa identificativa del tipo di messaggio
     */
    MsgType(final String c) {
        this.code = c;
    }

    /**
     * 
     * @return codifica del tipo di messaggio
     */
    public String getCode() {
        return this.code;
    }

    /**
     * 
     * @param c
     *            codice da ricercare
     * @return il tipo di messaggio che ha come codice la stringa c (null se
     *         questo non esiste)
     */
    public static MsgType get(final String c) {
        return Arrays
                .asList(MsgType.values())
                .stream()
                .filter(code -> code.getCode().equals(c))
                .findFirst()
                .orElse(null);
    }

}
