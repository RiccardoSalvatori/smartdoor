package smartdoor.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import smartdoor.common.event.ButtonPressed;
import smartdoor.common.event.Event;
import smartdoor.comunication.MsgHandler;
import smartdoor.devices.generic.Light;
import smartdoor.devices.generic.ObservableButton;
import smartdoor.common.event.Msg;
import smartdoor.common.event.MsgEvent;
import smartdoor.common.event.MsgType;

public class Gateway extends ReactiveAgent {

    private Light insideLed;
    private Light failedAccess;
    private MsgHandler messagesHandler;
    private final List<User> users = new ArrayList<>(Arrays.asList(new User("admin", "password")));

    private enum State {
        IDLE, CREDENTIAL_RECEIVED, USER_INSIDE
    };

    private State currentState;
    private User currentUser;

    private int temp = 0;
    private int lightValue = 0;
    

    public void init (final Light insideLed, final Light failedAccess, final MsgHandler messagesHandler) {
        this.messagesHandler = messagesHandler;
        this.insideLed = insideLed;
        this.failedAccess = failedAccess;
        this.currentState = State.IDLE;

    }

    @Override
    protected void processEvent(Event<?> ev) {
        if (updateReceived(ev)) {
            Logger.get().log("Update Received");
            final String received = ((MsgEvent) ev).getMsg().getString();
            final int[] values = parseValues(received);
            this.temp = values[0];
            this.lightValue = values[1];
        }
        switch (this.currentState) {
        case IDLE:
            if (credentialReceived(ev)) {
                final String received = ((MsgEvent) ev).getMsg().getString();
                final User credential = this.parseCredential(received);
                if (this.users.contains(credential)) {
                    this.sendMsgTo(this.messagesHandler, new Msg(MsgType.OK));
                    this.currentState = State.CREDENTIAL_RECEIVED;
                    this.currentUser = credential;
                } else {
                    this.sendMsgTo(this.messagesHandler, new Msg(MsgType.FAIL));
                    this.failedAccess.flash(500);
                    Logger.get().logFile("Failed Access - wrong credentials " + credential);
                }
            }

            break;
        case CREDENTIAL_RECEIVED:
            if (noPresenceDetected(ev)) {
                Logger.get().logFile("Session abort: no presence detected");
                this.failedAccess.flash(500);
                this.currentState = State.IDLE;
            } else if (userInside(ev)) {
                this.insideLed.switchOn();
                Logger.get().logFile("Session start - user:" + this.currentUser.getName() + " entered");
                this.currentState = State.USER_INSIDE;
            }
            break;

        case USER_INSIDE:
            if (close(ev)) {
                this.insideLed.switchOff();
                Logger.get().logFile("Session end - user:" + this.currentUser.getName() + " left the room");
                this.currentState = State.IDLE;
            }
            break;
        }

    }

    private int[] parseValues(final String received) {
        final String[] parsed = received.split(" ");
        final int[] values = { Integer.parseInt(parsed[0]), Integer.parseInt(parsed[1]) };
        return values;
    }

    private User parseCredential(final String credential) {
        final String[] parsed = credential.split(" ");
        return new User(parsed[0], parsed[1]);

    }

    private boolean credentialReceived(final Event<?> ev) {
        return ev instanceof MsgEvent && ((MsgEvent) ev).getMsg().getType().equals(MsgType.CHECK);
    }

    private boolean noPresenceDetected(final Event<?> ev) {
        System.out.println(((MsgEvent) ev).getMsg());
        return ev instanceof MsgEvent && ((MsgEvent) ev).getMsg().getType().equals(MsgType.NO_PRESENCE);
    }

    private boolean userInside(final Event<?> ev) {
        return ev instanceof MsgEvent && ((MsgEvent) ev).getMsg().getType().equals(MsgType.USER_INSIDE);
    }

    private boolean updateReceived(final Event<?> ev) {
        if(ev instanceof MsgEvent){
            System.out.println(((MsgEvent) ev).getMsg().getType());
        }
        return ev instanceof MsgEvent && ((MsgEvent) ev).getMsg().getType().equals(MsgType.UPDATE);
    }

    private boolean close(Event<?> ev) {
        return ev instanceof MsgEvent && ((MsgEvent) ev).getMsg().getType().equals(MsgType.OFF);
    }

    
    public int getTemp(){
        return this.temp;
    }
    
    public int getLightValue() {
        return this.lightValue;
    }

}
