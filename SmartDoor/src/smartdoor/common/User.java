package smartdoor.common;

public class User {
    
    private final String name;
    private final String password;
    
    
    
   
    public User(final String name, final String password) {
        this.name = name;
        this.password = password;
    }

    /**
     * @return the x
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return the y
     */
    public String getPassword() {
        return this.password;
    }
    
    public String toString() {
        return "user: " + this.name + " password: " + this.password;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof User)) {
            return false;
        }
        User other = (User) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (password == null) {
            if (other.password != null) {
                return false;
            }
        } else if (!password.equals(other.password)) {
            return false;
        }
        return true;
    }

    

}
