package smartdoor.comunication;

import smartdoor.common.Observer;
import smartdoor.common.ReactiveAgent;
import smartdoor.common.event.Event;
import smartdoor.common.event.Msg;
import smartdoor.common.event.MsgAvailable;
import smartdoor.common.event.MsgEvent;
import smartdoor.common.event.MsgType;

/**
 * Classe singleton. "instrada" i messaggi da raspberry ad arduino e viceversa.
 */
public final class MsgHandler extends ReactiveAgent implements Observer {
    private static final String SPLITTER = " ";

    private final ObservableSerial serial;
    private final ReactiveAgent agent;
    private static MsgHandler instance;

    private MsgHandler(final ObservableSerial serial, final ReactiveAgent agent) {
        this.serial = serial;
        this.agent = agent;
        this.serial.addObserver(this);
    }

    public static MsgHandler getInstance() {
        if (instance != null) {
            return instance;
        } else {
            throw new IllegalStateException();
        }
    }

    public static void init(final ObservableSerial serial, final ReactiveAgent radar) {
        instance = new MsgHandler(serial, radar);
    }

    @Override
    protected void processEvent(final Event<?> ev) {
        if (ev.getSource().equals(this.agent) && ev instanceof MsgEvent) {
            final Msg msg = ((MsgEvent) ev).getMsg();
            this.serial.sendMsg(msg.getType().getCode());
        } else if (ev.getSource().equals(this.serial) && ev instanceof MsgAvailable) {
            final String[] msg = ((MsgAvailable) ev).getMsg().split(SPLITTER, 2);
            final MsgType code = MsgType.get(msg[0]);
            final String info = msg.length > 1 ? msg[1] : " ";
            if (!code.equals(MsgType.LOG)) {
                this.sendMsgTo(this.agent, new Msg(info, code));
            } else {
                System.out.println(code + " : " + info);
            }
        }
    }

    /**
     * Manda un messaggio di chiusura sia a raspberry che arduino, poi chiude la
     * seriale
     */
    public void stopSystem() {
        this.serial.sendMsg(MsgType.OFF.getCode());
        this.sendMsgTo(this.agent, new Msg("", MsgType.OFF));
        this.serial.close();
    }

}
