package smartdoor.devices.generic;

public interface Button {

    boolean isPressed();

}
