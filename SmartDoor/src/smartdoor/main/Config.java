package smartdoor.main;

/**
 * Configurazione pin del sistema
 *
 */
public enum Config {

    INSIDE(3), FAILED(4);

    private final int pin;

    Config(final int p) {
        this.pin = p;
    }

    public int pin() {
        return this.pin;
    }

}
