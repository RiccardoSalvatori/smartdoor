package smartdoor.main;

import java.io.OutputStream;
import java.net.InetSocketAddress;
import com.sun.net.httpserver.HttpServer;
import smartdoor.common.Gateway;
import smartdoor.common.Logger;
import smartdoor.comunication.ObservableSerial;
import smartdoor.devices.pi4j.PhysicalLed;
import smartdoor.comunication.MsgHandler;

public class Main {
    private static final int BAUD_RATE = 9600;
    private static final String SERIAL_PORT = "/dev/ttyS99";
    private static final int SERVER_PORT = 1215;

    public static void main(final String[] args) throws Exception {
        final PhysicalLed failed = new PhysicalLed(Config.INSIDE.pin());
        final PhysicalLed inside = new PhysicalLed(Config.FAILED.pin());
        ObservableSerial serial;
        serial = new ObservableSerial(SERIAL_PORT, BAUD_RATE);
        final Gateway gateway = new Gateway();
        
        MsgHandler.init(serial, gateway);
        gateway.init(inside, failed, MsgHandler.getInstance());
        MsgHandler.getInstance().start();
        gateway.start();

        final HttpServer server = HttpServer.create(new InetSocketAddress(SERVER_PORT), 0);
        server.createContext("/smart-door", t -> {
            final String fileLog = Logger.get().getFileText().replace(System.getProperty("line.separator"), "<br>");
            final String response = "<h1> System Status </h1>" + "<section><h2> Measures </h2>" + "<p>temp = "
                    + gateway.getTemp() + "</p>" + "<p>light = " + gateway.getLightValue() + "</p></section>"
                    + "<section><h2>Log</h2><p>" + fileLog + "</p></section>";
            t.sendResponseHeaders(200, response.getBytes().length);
            final OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        });
        server.setExecutor(null); // creates a default executor
        server.start();
    }

}
