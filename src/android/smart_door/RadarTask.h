#ifndef __RADAR_TASK__
#define __RADAR_TASK__

#include "Task.h"
#include <Servo.h>
#include "ProximitySensor.h"
#include "Led.h"

class RadarTask: public Task {

public:
  RadarTask();
  void init(int period);  
  void tick();

private:
  int angle;
  ProximitySensor* pProx;
  Servo* pServo;
  Led* pConnected;
  enum { OFF, IDLE, MOVING, DETECTING, RESET } state;
};

#endif

