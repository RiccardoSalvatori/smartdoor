#ifndef __CONFIG__
#define __CONFIG__

#define LED_CONNECTED_PIN 13
#define PROX_TRIG_PIN 7
#define PROX_ECHO_PIN 8
#define SERVO_PIN 9

#endif
