#ifndef __SESSIONTASK__
#define __SESSIONTASK__

#include "Task.h"
#include "LightExt.h"
#include "PresenceSensor.h"
#include "TempSensor.h"
#include "Button.h"
#include "Context.h"


class SessionTask: public Task {

private:

  Button* exitButton;
  LightExt* led;
  PresenceSensor* presSensor;
  TempSensor* tempSensor; 
  int last_temp;

  Context* ctx;
  enum {IDLE, SESSION_START} state;

public:
  SessionTask(Context* context);  
  void init(int period);  
  void tick();
};

#endif
