#ifndef __CONTEXT__
#define __CONTEXT__

#include "NewPing.h"
#include "PresenceSensor.h"

#define DIST_CLOSE 50
#define DIST_MIN 10
#define DIST_MAX 100

/*
 * Classe per gestire i sensori e le variabili condivise tra i task.
 */
class Context {

public: 
  Context();

  void closeSession();
  void startSession();
  bool isSessionClosed();
  bool canStartSession();

  void setLoginResponse(String response);
  String getLoginResponse();

  void setLightRequest(int val);
  bool checkLightRequest();
  int getLedIntensity();
  
  void setTempRequest(bool b);
  bool checkTempRequest();

  void setExitRequest(bool b);
  bool checkExitRequest();

  void setCredentialsAvailable(bool b);
  bool isCredentialsAvailable();
  void setCredentials(String admin, String password);
  String getAdmin();
  String getPassword();
  



private:
    bool session;
    bool lightRequest;
    bool tempRequest;
    bool exitRequest;
    String response;
    int ledIntensity;
    String admin;
    String password;
    bool credentials;
};

#endif
