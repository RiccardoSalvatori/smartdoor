#ifndef __BTSERVICE__
#define __BTSERVICE__

#include "Arduino.h"
#include "MsgService.h"
#include "SoftwareSerial.h"



class BluetoothService : public MsgService {
    
public: 
  
  BluetoothService(int rxPin, int txPin);  
  void init();  
  bool isMsgAvailable();
  Msg* receiveMsg();
  void send(const String& msg);

private:
  String content;
  SoftwareSerial* channel;
};

extern BluetoothService bluetooth;

#endif

