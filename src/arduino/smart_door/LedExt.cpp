#include "LedExt.h"
#include "Arduino.h"

LedExt::LedExt(int pin) : Led(pin) {
  currentIntensity = 0;
  isOn = false;
}

LedExt::LedExt(int pin, int intensity) : Led(pin) {
  isOn = false;
 currentIntensity = check(intensity);
}

void LedExt::switchOn(){
  analogWrite(pin,currentIntensity);
  isOn = true;
}

int LedExt::getIntensity() {
  return this->currentIntensity;
}

void LedExt::setIntensity(int value){
    currentIntensity = check(value);  
    if (isOn){
      analogWrite(pin,currentIntensity);   
      }
}

void LedExt::switchOff(){
  analogWrite(pin,0);
  isOn = false;
}

/**
 * Ritorna sempre una valore compreso tra 0 e MAX_INT
 */
int LedExt::check(int intensity){
  if(intensity<0){
    return 0;
  } else if(intensity>MAX_INT){
    return MAX_INT;
  } else {
    return intensity;
  }
}

