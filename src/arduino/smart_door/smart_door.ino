
#include "Scheduler.h"
#include "CommTask.h"
#include "DoorTask.h"
#include "SessionTask.h"
#include "ServoTimer2.h"
#include "Context.h"
#include "Logger.h"
#include "SerialService.h"
#include "BluetoothService.h"
#include "config.h"


#define SCHED_TIME 40
#define COMM_TSK_PERIOD 40
#define DOOR_TSK_PERIOD 80
#define SESSION_TSK_PERIOD 120

Scheduler sched;

ServoTimer2* servo = new ServoTimer2();


void setup() {

  Context* context = new Context();
  serial.init();
  bluetooth.init();
  
  DoorTask* door_task = new DoorTask(servo, context);
  SessionTask* session_task = new SessionTask(context);
  CommTask* comm_task = new CommTask(context);
  
  comm_task->init(COMM_TSK_PERIOD);
  door_task->init(DOOR_TSK_PERIOD);
  session_task->init(SESSION_TSK_PERIOD);
  sched.init(SCHED_TIME); 
  

  sched.addTask(comm_task);
  sched.addTask(door_task);
  sched.addTask(session_task);


  while(!Serial){};
  Logger.log("Ready!");
}

void loop() {
    sched.run();
}
