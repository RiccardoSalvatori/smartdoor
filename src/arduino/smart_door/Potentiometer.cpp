#include "Potentiometer.h"
#include "Arduino.h"

Potentiometer::Potentiometer(int pin) {
  this->pin = pin;
}

int Potentiometer::getValue() {
  return analogRead(pin);
}

int Potentiometer::getBoundedValue(int min, int max) {
  return map(analogRead(pin), 0, MAX_VALUE, min, max);
};
