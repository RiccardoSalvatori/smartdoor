#include "DoorTask.h"
#include "Arduino.h"
#include "CommTask.h"
#include "SerialService.h"
#include "BluetoothService.h"
#include "config.h"
#include "MsgType.h"
#include "Logger.h"
#include "ServoTimer2.h"
#include "SoftwareSerial.h"


#define SERVO_STEP 10
#define DOOR_OPEN_ANGLE 175
#define DOOR_CLOSE_ANGLE 5

#define MIN_DIST 8
#define MIN_SEC 2000

unsigned long time_t = 0;

DoorTask::DoorTask(ServoTimer2* servo, Context* ctx) {

  this->proxSensor = new NewPing(TRIG, ECHO, MAX_DISTANCE);
  this->ctx = ctx;
  this->servo = servo;
  this->servoAngle = DOOR_CLOSE_ANGLE;
}

void DoorTask::init(int period) {
  Task::init(period);
  this->state = CLOSE;
}

void DoorTask::tick() {

  unsigned int currDistance = NewPing::convert_cm(this->proxSensor->ping_median());
  const String& response = "";
  
  switch(this->state){
    case CLOSE:
      if(currDistance < MIN_DIST){
        time_t = millis();
        this->state = USER_APPROACHING;    
      }
      
    break;
    
    case USER_APPROACHING:
    
      if(millis() - time_t > MIN_SEC){
        bluetooth.send(HELO);
        this->state = CHECK_LOGIN;      
      }
  
      if(currDistance > MIN_DIST){
        time_t = 0;
        this->state = CLOSE;
      }
    break;

    case CHECK_LOGIN:
       if(this->ctx->isCredentialsAvailable()){      
         serial.send(CHECK(this->ctx->getAdmin(), this->ctx->getPassword()));
         this->ctx->setCredentialsAvailable(false);
       }

       response = this->ctx->getLoginResponse();
       if(response.equals(CHECK_OK)){
         this->servo->attach(SERVO_PIN);
         bluetooth.send(CHECK_OK);
         this->state = OPENING;
       } else if(response.equals(CHECK_FAIL)) {
           this->ctx->setCredentialsAvailable(false);
           this->ctx->setCredentials("","");
           bluetooth.send(CHECK_FAIL);
       }
    
     if(currDistance > MIN_DIST){
      time_t = 0;
      this->state = CLOSE;
     }
    break;
    
    case OPENING: 
      if(this->servoAngle >= DOOR_OPEN_ANGLE ){
        this->servo->detach();
        this->ctx->startSession();
        this->state = OPEN;
      } else {
        this->servoAngle += SERVO_STEP;
        this->servo->write(map(this->servoAngle,0, 180, MIN_PULSE_WIDTH, MAX_PULSE_WIDTH));
      }
    break;
    
    case OPEN: 
      if(this->ctx->isSessionClosed()){
        this->servo->attach(SERVO_PIN);   
        this->state = CLOSING;
      } 
      break;
    
    case CLOSING:
      this->servoAngle =  map(this->servo->read(), MIN_PULSE_WIDTH, MAX_PULSE_WIDTH, 0, 180);
      if(this->servoAngle <= DOOR_CLOSE_ANGLE ){
        this->servo->detach();
        this->state = CLOSE;
      } else {
        this->servo->write(map(this->servoAngle - SERVO_STEP,0, 180, MIN_PULSE_WIDTH, MAX_PULSE_WIDTH));
      }
    break;

  }
}




