#ifndef __MSG_TYPE__
#define __MSG_TYPE__


#define CHECK(usr,psw) "CHECK " + String(usr) + " " + String(psw)
#define NO_PRESENCE    "NOPRES"
#define USER_INSIDE    "INSIDE"


#define CHECK_FAIL "0"
#define CHECK_OK   "1"
#define LOGIN      "2|" 
#define LED_SET    "3|"
#define HELO       "4"

#define UPDATE(temp, intensity) "5| " + String(temp) + " " + String(intensity)
#define SESSION_CLOSE           "6"



#endif
