#ifndef __CONFIG__
#define __CONFIG__

#define BTN_EXIT_PIN 4
#define LED_VALUE_PIN 6
#define SERVO_PIN 9
#define PIR_PIN 10
#define ECHO 11
#define TRIG 12
#define MAX_DISTANCE 400

//Bluetooth
#define RX_PIN 2
#define TX_PIN 3

#endif
