#ifndef __MSGSERVICE__
#define __MSGSERVICE__

#include "Arduino.h"

class Msg {
  String content;

public:
  Msg(String content){
    this->content = content;
  }
  
  String getContent(){
    return content;
  }
};

/*
 * Classe astratta per la gestione delle comunicazioni.
 */
class MsgService{

public:
  virtual void init() = 0;  
  virtual bool isMsgAvailable() = 0;
  virtual Msg* receiveMsg() = 0;
  virtual void send(const String& msg) = 0;
  
};

#endif

