 #include "Context.h"

Context::Context() {
  this->session = false;
  this->lightRequest = false;
  this->tempRequest = false;
  this->exitRequest = false;
  this->response = "";
  this->credentials = false;
  this->admin = "";
  this->password = "";
}



//SESSION
bool Context::canStartSession(){
  return this->session;
}

void Context::startSession(){
  this->session = true;
}

void Context::closeSession(){
  this->session = false;
}

bool Context::isSessionClosed(){
  return this->session == false;
}


//LOGIN
void Context::setLoginResponse(String response){
  this->response = response;
}

String Context::getLoginResponse(){
  String tmp = this->response;
  this->response = "";
  return tmp;
}


//USER 
void Context::setLightRequest(int val){
  this->lightRequest = true;
  val = map(val,0,100,0,255);
  this->ledIntensity = val;
}

bool Context::checkLightRequest(){
  return this->lightRequest;
}

int Context::getLedIntensity(){
  this->lightRequest = false;
  return this->ledIntensity;
}

void Context::setTempRequest(bool b){
  this->tempRequest = b;
}


bool Context::checkTempRequest(){
  return this->tempRequest;
}

void Context::setExitRequest(bool b){
  this->exitRequest = b;
}

bool Context::checkExitRequest(){
  return this->exitRequest;
}

void Context::setCredentialsAvailable(bool b){
  this->credentials = b;
}

bool Context::isCredentialsAvailable(){
  return this->credentials;
}

void Context::setCredentials(String admin, String password){
  this->admin = admin;
  this->password = password;
}

String Context::getAdmin(){
  return this->admin;
}

String Context::getPassword(){
  return this->password;
}



