#ifndef __COMMTASK__
#define __COMMTASK__

#include "Task.h"
#include "Context.h"
#include "Arduino.h"


class CommTask : public Task{
private:
  Context* context;

public:
  CommTask(Context* context);
	void init(int period);
	void tick();

};

#endif



