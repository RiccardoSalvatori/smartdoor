#include "Pir.h"
#include "Arduino.h"

boolean presence = false;

Pir::Pir(int pin) {
	this->pin = pin;
	pinMode(pin, INPUT);
	digitalWrite(pin, LOW);
}

bool Pir::presenceDetected() {
  return digitalRead(pin) == HIGH;
}
