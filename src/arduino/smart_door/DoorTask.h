#ifndef __DOORTASK__
#define __DOORTASK__

#include "Task.h"
#include "ServoTimer2.h"
#include "Context.h"

#include "NewPing.h"

class DoorTask: public Task {

private:


  NewPing* proxSensor;
  ServoTimer2* servo;
  int servoAngle;

  Context* ctx;
  enum { CLOSE, USER_APPROACHING, CHECK_LOGIN, OPENING, OPEN, CLOSING} state;

public:
  DoorTask(ServoTimer2* servo, Context* context);  
  void init(int period);  
  void tick();
};

#endif
