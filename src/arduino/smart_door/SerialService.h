#ifndef __SERIALSERVICE__
#define __SERIALSERVICE__

#include "Arduino.h"
#include "MsgService.h"

class SerialService : public MsgService{
    
public: 
  
  Msg* currentMsg;
  bool msgAvailable;

  void init();  
  bool isMsgAvailable();
  Msg* receiveMsg();
  void send(const String& msg);
};

extern SerialService serial;

#endif

