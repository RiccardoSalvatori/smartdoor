
#include "SessionTask.h"
#include "Arduino.h"
#include "LedExt.h"
#include "Pir.h";
#include "ButtonImpl.h"
#include "CommTask.h"
#include "config.h";
#include "SerialService.h"
#include "BluetoothService.h"
#include "MsgType.h";
#include "Logger.h"



#define TEMP_ERR 1
#define MAX_DELAY 8000 



unsigned long timer = 0;
int i = 1;
int seconds = 0;

SessionTask::SessionTask(Context* context) {
  this->ctx = context;
  this->exitButton = new ButtonImpl(BTN_EXIT_PIN);
  this->tempSensor = new TempSensor();
  this->led = new LedExt(LED_VALUE_PIN);
  this->presSensor = new Pir(PIR_PIN);
  this->last_temp = 0;
  
}

void SessionTask::init(int period) {
  Task::init(period);
  this->state = IDLE;
}

void SessionTask::tick() {
  int current_temp =  this->tempSensor->readTemperature();
  
  switch (this->state) {
    case IDLE:
      if (this->ctx->canStartSession()) {
        this->led->switchOn();
        serial.send(USER_INSIDE);
        this->state = SESSION_START;
      }

      break;
    case SESSION_START:
      timer += this->getPeriod();
      if (this->presSensor->presenceDetected()) {
        timer = 0;
      }

         
      if(this->ctx->checkExitRequest() || this->exitButton->isPressed() || timer >= MAX_DELAY){
        if(timer >= MAX_DELAY){
           serial.send(NO_PRESENCE);
        }

        if(this->ctx->checkExitRequest()){
          this->ctx->setExitRequest(false);
        }
        this->ctx->closeSession();
        timer = 0;
        serial.send(SESSION_CLOSE);
        bluetooth.send(SESSION_CLOSE);
        this->state = IDLE;
       
      } else if (this->ctx->checkTempRequest()) {
        this->ctx->setTempRequest(false);
        bluetooth.send(String(current_temp));
      } else if (this->ctx->checkLightRequest()){
        this->led->setIntensity(this->ctx->getLedIntensity());
        serial.send(UPDATE(current_temp, this->led->getIntensity()));
      }

      break;
  }

  if (current_temp < last_temp - TEMP_ERR || current_temp > last_temp + TEMP_ERR) {
    last_temp = current_temp;
    serial.send(UPDATE(current_temp, this->led->getIntensity()));
    bluetooth.send(UPDATE(current_temp, this->led->getIntensity()));
  }
}
