#include "Arduino.h"
#include "SerialService.h"

String content;

SerialService serial;

bool SerialService::isMsgAvailable(){
  return msgAvailable;
}

Msg* SerialService::receiveMsg(){
  if (msgAvailable){
    Msg* msg = currentMsg;
    msgAvailable = false;
    currentMsg = NULL;
    content = "";
    return msg;  
  } else {
    return NULL; 
  }
}

void SerialService::init(){
  Serial.begin(9600);
  content.reserve(256);
  content = "";
  currentMsg = NULL;
  msgAvailable = false;  
}

void SerialService::send(const String& msg){
  Serial.println(msg);  
}

void serialEvent() {
  /* reading the content */
  while (Serial.available()) {
    char ch = (char) Serial.read();
    if (ch == '\n'){
      serial.currentMsg = new Msg(content);
      serial.msgAvailable = true;    
    } else {
      content += ch;      
    }
  }
}
  


