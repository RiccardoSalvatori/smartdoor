#include "CommTask.h"
#include "SerialService.h"
#include "BluetoothService.h"
#include "MsgType.h"
#include "Logger.h"
#include "Arduino.h"

void CommTask::init(int period) {
	Task::init(period);
}

CommTask::CommTask(Context* context) {
  this->context = context;
}

void CommTask::tick() {
  if (serial.isMsgAvailable()){
    Msg* msg = serial.receiveMsg(); 
    const String& content = msg->getContent();
    if(content.equals(CHECK_OK)){
      this->context->setLoginResponse(CHECK_OK);
    } else if (content.equals(CHECK_FAIL)){
      this->context->setLoginResponse(CHECK_FAIL);
    } 
    delete msg;    
  }

  if (bluetooth.isMsgAvailable()) {
    Msg* msg = bluetooth.receiveMsg();
    const String& content = msg->getContent();
    if(content.indexOf(LED_SET) != -1){
       content.replace(LED_SET, "");
       this->context->setLightRequest(content.toInt());
    } else if(content.equals(SESSION_CLOSE)){
       this->context->setExitRequest(true);
    } else if(content.indexOf(LOGIN) != -1){
       content.replace(LOGIN, "");
       const String& admin = content.substring(0,content.indexOf(" "));
       const String& password = content.substring(content.indexOf(" ") + 1);
       this->context->setCredentialsAvailable(true);
       this->context->setCredentials(admin, password);
    }
    delete msg;
  }
}


