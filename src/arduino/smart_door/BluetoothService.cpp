#include "Arduino.h"
#include "BluetoothService.h"
#include "Logger.h"
#include "config.h"

BluetoothService bluetooth(RX_PIN,TX_PIN);

BluetoothService::BluetoothService(int rxPin, int txPin){
  this->channel = new SoftwareSerial(rxPin, txPin);
}

bool BluetoothService::isMsgAvailable(){
  return channel->available();
}

Msg* BluetoothService::receiveMsg(){
  if (channel->available()){    
    content="";
    while (channel->available()) {
      content += (char)channel->read();      
    }
    return new Msg(content);
  } else {
    return NULL;  
  }
}

void BluetoothService::init(){
  content.reserve(256);
  channel->begin(9600); 
}

void BluetoothService::send(const String& msg){
  channel->println(msg); 
}




