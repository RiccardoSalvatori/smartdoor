package lab.seiot.unibo.it.smartdoor;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.Set;
import java.util.UUID;

import static android.content.ContentValues.TAG;

public class MainActivity extends Activity {

    private static final String APP_UUID = "00001101-0000-1000-8000-00805F9B34FB";
    private BluetoothAdapter btAdapter ;

    private final int ENABLE_BT_REQ = 1;
    private Set<BluetoothDevice> nbDevices = null;
    private static MainHandler mainHandler;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        mainHandler = new MainHandler(this);
        /* Initialize BT ... */

        if(btAdapter == null){
            Log.e(" MyApp ","BT is not available on this device ");
            finish();
        }

        if (!btAdapter.isEnabled()) {
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 0);
        }

        Button btn = (Button) findViewById(R.id.sign_in_button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                String username = ((TextView) findViewById(R.id.username)).getText().toString();
                String password = ((TextView) findViewById(R.id.password)).getText().toString();
                sendMessage("LOGIN|" + username + " " + password);
            }
        });

        Button cnt = (Button) findViewById(R.id.connect);
        cnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                String BT_TARGET_NAME = "isi05";
                BluetoothDevice targetDevice = null ;
                Set<BluetoothDevice>pairedList = btAdapter.getBondedDevices();
                if(pairedList.size() > 0) {
                    for(BluetoothDevice device:pairedList){
                        Log.e(TAG, "DEVICE: " + device.getName());
                        if(device.getName().equals(BT_TARGET_NAME))
                            targetDevice = device ;
                    }
                }
                Log.e(TAG, "DEVICE: " + targetDevice.getName());
                UUID uuid = UUID.fromString(APP_UUID);
                new ConnectionTask(targetDevice, uuid).execute();
            }
        });

    }

    public void onStart() {
        super.onStart();
    }

    public void onDestroy() {
        super.onDestroy();
        ConnectionManager.getInstance().cancel();
    }

    private void sendMessage (String msg) {
        ConnectionManager.getInstance().write(msg.getBytes());
    }

    public static MainHandler getHandler(){
        return mainHandler;
    }

    public class MainHandler extends Handler {

        private final WeakReference<MainActivity> context;

        MainHandler(MainActivity context){
            this.context = new WeakReference<>(context);
        }

        public void handleMessage(final String msg) {

            if(msg.equals("OK")) {
                System.out.print("OK");
                startActivity(new Intent(MainActivity.this, ControlActivity.class));
            } else if(msg.equals("FAIL")) {
                System.out.print("FAIL");
            } else {
                System.out.print(msg);

            /*TextView txt = (TextView) context.get().findViewById(R.id.readMessage);
            txt.setText(msg);*/
                /*runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((TextView) context.get().findViewById(R.id.readMessage)).setText(msg);
                    }
                });*/
            }
        }

    }

}
