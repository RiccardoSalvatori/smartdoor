package lab.seiot.unibo.it.smartdoor;

import android.app.Activity;
import android.bluetooth.BluetoothSocket;
import android.os.Looper;
import android.widget.TextView;
import java.lang.Runnable;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ConnectionManager extends Thread {

    private BluetoothSocket btSocket;
    private InputStream btInStream;
    private OutputStream btOutStream;
    private boolean stop;
    private static ConnectionManager instance = null;

    public ConnectionManager() {
        stop = true;
    }

    public static ConnectionManager getInstance() {
        if(instance == null) {
            instance = new ConnectionManager();
        }
        return instance;
    }

    public void setChannel(BluetoothSocket socket) {
        btSocket = socket;
        try {
            btInStream = socket.getInputStream();
            btOutStream = socket.getOutputStream();
        } catch ( IOException e) { /* ... */ }
        stop = false;
    }

    public void run (){
        byte[] buffer = new byte[1024];
        int nBytes = 0;
        while (!stop){
            try {
                nBytes = btInStream.read(buffer);
                /* Manage here the data stored in buffer */
                final String readMessage = new String(buffer, 0, nBytes);
                MainActivity.getHandler().handleMessage(readMessage);
            } catch (IOException e) {
                stop = true ;
            }
        }
    }

    public boolean write (byte[] bytes) {
        if( btOutStream == null ) {
            return false;
        }
        try{
            btOutStream.write(bytes);
        } catch (IOException e){
            return false ;
        }
        return true ;
    }

    public void cancel() {
        try{
            btSocket.close();
        } catch (IOException e) { /* ... */ }
    }



}