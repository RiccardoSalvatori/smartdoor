package lab.seiot.unibo.it.smartdoor;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.lang.ref.WeakReference;

public class ControlActivity extends AppCompatActivity {

    private static ControlHandler controlHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);

        SeekBar sk = (SeekBar) findViewById(R.id.seekBar);
        sk.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub
            }
        });

        Button exit = (Button) findViewById(R.id.exit_button);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                sendMessage("CLOSE");
            }
        });

    }

    public void onStart() {
        super.onStart();
    }

    private void sendMessage (String msg) {
        ConnectionManager.getInstance().write(msg.getBytes());
    }

    public static ControlHandler getHandler(){
        return controlHandler;
    }

    public class ControlHandler extends Handler {

        private final WeakReference<ControlActivity> context;

        ControlHandler(ControlActivity context){
            this.context = new WeakReference<>(context);
        }

        public void handleMessage(final String msg) {

            if(msg.equals("RICEVO_TEMPERATURA")) {
                //Qui probabilmente bisogna lanciare un thread, come faceva Ste!!!
                ((TextView) context.get().findViewById(R.id.temperature)).setText("Temperature: " + msg);
            } else {
                System.out.print(msg);
            }
        }

    }

}
